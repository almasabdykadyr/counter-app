package com.almasabdykadyr.counterapp

import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import android.util.Log
import android.widget.Button

class MainActivity : AppCompatActivity() {
    private lateinit var counterService: CounterService

    private lateinit var incrementButton: Button
    private lateinit var decrementButton: Button
    private lateinit var showButton: Button

    private val connection = object : ServiceConnection {
        override fun onServiceConnected(name: ComponentName?, service: IBinder?) {
            Log.i(LOG_TAG, "Connecting service")
            val binder = service as CounterService.MyServiceBinder
            counterService = binder.getService()
        }

        override fun onServiceDisconnected(name: ComponentName?) {
            Log.i(LOG_TAG, "Disconnecting service")
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        incrementButton = findViewById(R.id.incrementValueButton)
        decrementButton = findViewById(R.id.decrementValueButton)
        showButton = findViewById(R.id.showButton)
    }

    override fun onStart() {
        super.onStart()
        Intent(this, CounterService::class.java).also { intent ->
            bindService(intent, connection, Context.BIND_AUTO_CREATE)
        }

        incrementButton.setOnClickListener {
            counterService.incrementValue()
            Log.i(LOG_TAG, "Value incremented")
        }

        decrementButton.setOnClickListener {
            counterService.decrementValue()
            Log.i(LOG_TAG, "Value decremented")
        }

        showButton.setOnClickListener {
            counterService.showToast()
            Log.i(LOG_TAG, "Toast shown")
        }
    }


    override fun onStop() {
        super.onStop()
        unbindService(connection)
    }
}