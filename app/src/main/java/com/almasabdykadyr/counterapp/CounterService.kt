package com.almasabdykadyr.counterapp

import android.app.Service
import android.content.Intent
import android.os.Binder
import android.os.IBinder
import android.widget.Toast

class CounterService : Service() {

    private val binder = MyServiceBinder()
    private var count = 0

    override fun onBind(intent: Intent): IBinder {
        return binder
    }

    fun incrementValue() {
        count++
    }

    fun decrementValue() {
        count--
    }

    fun showToast() {
        Toast.makeText(applicationContext, count.toString(), Toast.LENGTH_SHORT).show()
    }

    inner class MyServiceBinder: Binder() {
        fun getService() = this@CounterService
    }
}